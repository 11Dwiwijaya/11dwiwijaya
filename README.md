<h1 align="center">Hi 👋, I'm Dwi Wijaya</h1>
<h3 align="center">I'm a 18 y.o Junior Web Developer, from Surakarta</h3>

<br>

- 🎓 I'm a fresh graduated from **SMKN 2 Karanganyar**

- 🌱 I’m currently learning **Yii2 framework**

- ⌨️ I'm using PHP (**Codeigniter, Yii2**), CSS (**Bootstrap** and **Tailwind**) and Database(**MySQL**)

- ⚡ Fun fact **I also like to learn bout Grapics Design**

- 📫 How to reach me **11dwiwijaya@gmail.com**
